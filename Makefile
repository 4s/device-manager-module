ZMQ_INCLUDE=/usr/local/include
ZMQ_LIB=/usr/local/lib

PROTOBUF_INCLUDE=/usr/local/include
PROTOBUF_LIB=/usr/local/lib

BASEPLATE_INCLUDE=../phg-native-baseplate/include
BASEPLATE_OBJS=../phg-native-baseplate/lib

MESSAGES_INCLUDE=../messages

CPPFLAGS=-DDEBUG

CXX=g++
RMR=rm -rf
CXXFLAGS=-std=c++11 -Wall -Wpedantic -Iinclude -I$(ZMQ_INCLUDE) -I$(PROTOBUF_INCLUDE) -I$(BASEPLATE_INCLUDE) -I$(MESSAGES_INCLUDE) -g -O0
LDFLAGS=-L$(ZMQ_LIB) -L$(PROTOBUF_LIB) $(BASEPLATE_OBJS)/*.o -pthread
LDLIBS=-lm -lzmq -lstdc++ -lprotobuf
OUTPUT_OPTION=-MMD -MP -o $@ 

COMMON=$(wildcard src/*.cpp) $(wildcard $(MESSAGES_INCLUDE)/*.cpp) 

POC=poc/proof-of-concept
POCSRC=$(wildcard poc/*.cpp) $(COMMON)
POCOBJS=$(POCSRC:.cpp=.o) 

ALL=$(COMMON) $(POCSRC)
OBJS=$(ALL:.cpp=.o)
DEPS=$(ALL:.cpp=.d)

$(POC): $(POCOBJS) 


.PHONY: all
all: $(POC) 

.PHONY: clean
clean:
	$(RM) $(OBJS) $(DEPS) $(POC) 

.PHONY: distclean
distclean: clean
	$(RM) $(POC)

.PHONY: proto
proto: 
	protoc -I=$(MESSAGES_INCLUDE) --cpp_out=$(MESSAGES_INCLUDE) $(MESSAGES_INCLUDE)/*.proto ;  \
    rm $(MESSAGES_INCLUDE)/DeviceControl_P2C.pb.cpp ; mv $(MESSAGES_INCLUDE)/DeviceControl_P2C.pb.cc $(MESSAGES_INCLUDE)/DeviceControl_P2C.pb.cpp ; \
	rm $(MESSAGES_INCLUDE)/DeviceManagement_C2P.pb.cpp ; mv $(MESSAGES_INCLUDE)/DeviceManagement_C2P.pb.cc $(MESSAGES_INCLUDE)/DeviceManagement_C2P.pb.cpp ; \
	rm $(MESSAGES_INCLUDE)/DeviceManagement_P2C.pb.cpp ; mv $(MESSAGES_INCLUDE)/DeviceManagement_P2C.pb.cc $(MESSAGES_INCLUDE)/DeviceManagement_P2C.pb.cpp ; \
	rm $(MESSAGES_INCLUDE)/Dim_C2P.pb.cpp ; mv $(MESSAGES_INCLUDE)/Dim_C2P.pb.cc $(MESSAGES_INCLUDE)/Dim_C2P.pb.cpp ; \
	rm $(MESSAGES_INCLUDE)/Dim_P2C.pb.cpp ; mv $(MESSAGES_INCLUDE)/Dim_P2C.pb.cc $(MESSAGES_INCLUDE)/Dim_P2C.pb.cpp ; 
	

