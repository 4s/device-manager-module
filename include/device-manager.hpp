/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The 4S PHG C++ Device Manager subsystem. The Device Manager is based
 *   on s4::BasePlate::ModuleBase and is intended to be used in a setup with
 *   other similar modules.
 *   The Device Manager:
 *     - keeps track of connected devices
 *     - keeps track of queries of devices matching filter criterias
 *     - queries DIMs for queried devices 
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef DEVICEMANAGER_HPP
#define DEVICEMANAGER_HPP

#include "Common.hpp"
#include "Function.hpp"
#include "ModuleBase.hpp"

#include "DeviceManagement_C2P.pb.h"
#include "DeviceControl_P2C.pb.h"
#include "DIM_P2C.pb.h"


namespace s4 {
  namespace device_manager {

    class DeviceManager : public s4::BasePlate::ModuleBase {
    private:
      class impl; std::shared_ptr<impl> pImpl; // pImpl pattern

      void handleDeviceConnected(std::shared_ptr<impl> _impl, s4::messages::device_control::DeviceConnected* m);
      void handleDeviceDisconnected(std::shared_ptr<impl> _impl, s4::messages::device_control::DeviceDisconnected* m);
      void handleSearchDevices(std::shared_ptr<impl> _impl, s4::messages::device_management::SearchDevices* m);
      void handleCancelSearch(std::shared_ptr<impl> _impl, s4::messages::device_management::CancelSearch* m);
      void handleFilteredDevice(std::shared_ptr<impl> _impl, s4::messages::dim::FilteredDevice* m);
      
    public:
      DeviceManager(s4::BasePlate::Context& context);
    };
    
  }
}

#endif // DEVICEMANAGER_HPP
