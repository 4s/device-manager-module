/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The 4S PHG C++ Device Manager device abstraction.
 *
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2018 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef DEVICE_HPP
#define DEVICE_HPP

#include <string>

namespace s4 {
  namespace device_manager {
    
    class Device final {
    public:
      std::string managedId;    // Id as registered device
      std::string deviceId;     // TODO - String/Guid/byte[] ?
      std::string hardwareType; // TODO - enum ?
      std::string hardwareInfo; // TODO - class/struct/json ?

      Device(const std::string& registeredId, const std::string& palDeviceId, const std::string& palHardwareType, const std::string& palHardwareInfo);

      std::string toString();
    };
  }
}

#endif // DEVICE_HPP
