/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief The 4S PHG C++ device manager subsystem. Internal manager decoupled 
 *   from messaging.
 * 
 *   The internal device manager is a simple abstraction over two maps of
 *   connected devices and current queries. 
 * 
 *   The manager is prepared for handling registering and unregistering
 *   devices. The PAL module knows nothing about registered devices and will
 *   never provide a managedId. The Device Manager persist the list of 
 *   registered devices without the PAL deviceId but with their unique
 *   managedId. When registering a device a new UUID is assigned as managedId.
 * 
 *   When a device is connected the device manager checks whether or not it 
 *   is registered; if so the managedId will amended to the device struct.
 *
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#ifndef INTERNAL_DEVICEMANAGER_H
#define INTERNAL_DEVICEMANAGER_H

#include "Common.hpp"

#include "device.hpp"

#include "DeviceManagement_C2P.pb.h"
#include "DeviceManagement_P2C.pb.h"
#include "DeviceControl_P2C.pb.h"
#include "DIM_C2P.pb.h"
#include "DIM_P2C.pb.h"

#include "ErrorMessage.pb.h" 
#include "LogMessage.pb.h"


namespace s4 {
  namespace device_manager {

    class InternalDeviceManager {
    private:
      std::map<std::string, Device> registeredDevices;          // key: managedId, value: Device
      std::map<std::string, Device> connectedDevices;           // key: deviceId, value: Device
      std::map<std::string, std::set<std::string>> queries;     // key: filter,  value: deviceId

    public:
      InternalDeviceManager() { };
      virtual ~InternalDeviceManager() {};

      void handleDeviceConnected(
        const s4::messages::device_control::DeviceConnected& m, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      void handleDeviceDisconnected(
        const s4::messages::device_control::DeviceDisconnected& m, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::dim::SetFilter> handleSearchDevices(
        const s4::messages::device_management::SearchDevices& m, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::dim::ClearFilter> handleCancelSearch(
        const s4::messages::device_management::CancelSearch& m, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::device_management::AvailableDevices> handleFilteredDevice(
        const s4::messages::dim::FilteredDevice& m, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::dim::SetFilter> makeSetFilter(
        const std::string& filter, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::dim::ClearFilter> makeClearFilter(
        const std::string& filter, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::unique_ptr<s4::messages::device_management::AvailableDevices> makeAvailableDevices(

        const std::string& queryId, 
        std::function<void(s4::BasePlate::LogMessage_Level, const std::string&, const std::string&, int)> logger);

      std::string getManagedId(const Device& device);

      std::map<std::string, Device> listRegisteredDevices();

      std::map<std::string, Device> listConnectedDevices();

      std::map<std::string, std::set<std::string>> listQueries(); 
    };
    
  }
}

#endif // INTERNAL_DEVICEMANAGER_H
