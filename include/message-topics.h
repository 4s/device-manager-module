#ifndef __MESSAGE_TOPICS_H__
#define __MESSAGE_TOPICS_H__

#define DEVICE_CONNECTED    "DeviceControl.P2C.DeviceConnected"
#define DEVICE_DISCONNECTED "DeviceControl.P2C.DeviceDisconnected"

#define SEARCH_DEVICES      "DeviceManagement.C2P.SearchDevices"
#define CANCEL_SEARCH       "DeviceManagement.C2P.CancelSearch"
#define AVAILABLE_DEVICES   "DeviceManagement.P2C.AvaliableDevices"

#define SET_FILTER          "Dim.C2P.SetFilter"
#define CLEAR_FILTER        "Dim.C2P.ClearFilter"
#define FILTERED_DEVICE     "Dim.P2C.FilteredDevice"

#endif //__MESSAGE_TOPICS_H__
