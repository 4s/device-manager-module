#ifndef __MESSAGE_TOPICS_H__
#define __MESSAGE_TOPICS_H__

#define DEVICE_CONNECTED    "event.DeviceConnected"
#define DEVICE_DISCONNECTED "event.DeviceDisconnected"

#define SEARCH_DEVICES      "event.SearchDevices"
#define CANCEL_SEARCH       "event.CancelSearch"
#define AVAILABLE_DEVICES   "event.AvaliableDevices"

#define SET_FILTER          "event.SetFilter"
#define CLEAR_FILTER        "event.ClearFilter"
#define FILTERED_DEVICE     "event.FilteredDevice"

#endif //__MESSAGE_TOPICS_H__