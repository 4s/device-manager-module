/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief Minimal Device Manager proof-of-concept.
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */

#include "device-manager.h"

#include "Common.hpp"
#include "BasePlate.hpp"
#include "ModuleBase.hpp"

#include "message-topics.h"
#include "DeviceManagement_P2C.pb.h"
#include "Dim_C2P.pb.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <chrono>
#include <thread>


using namespace s4::BasePlate;

namespace s4 {
  namespace DeviceManager {
    namespace Demo {

      class FakePalModule : public ModuleBase {

        void sendDeviceConnected(const std::string& deviceId, const std::string& hwType, const std::string& hwInfo) {
          auto connected = DeviceConnectedMsg();
          connected.set_device_id(deviceId);
          connected.set_hw_type(hwType);
          connected.set_hw_info(hwInfo);

          sendMulticast(DEVICE_CONNECTED, &connected);
        }

        void sendDeviceDisconnected(const std::string& deviceId) {
          auto disconnected = DeviceDisconnectedMsg();
          disconnected.set_device_id(deviceId);
              
          sendMulticast(DEVICE_DISCONNECTED, &disconnected);
        }

      public:
        FakePalModule(Context* context, peer_t id) : ModuleBase(context, id) {
          start();
        }

        void simulateDeviceConnect(const std::string& id, const std::string& type, const std::string& info) {
          dbg("Faking connect: " + id);
          sendDeviceConnected(id, type, info); 
        }

        void simulateDeviceDisconnect(const std::string& id) {
          dbg("Faking disconnect: " + id);
          sendDeviceDisconnected(id); 
        }
      };

      class FakeApp : public ModuleBase {

        void sendSearchDevices(const std::string& filter) {
          auto search = SearchDevicesMsg();
          search.set_md_code(filter);

          sendMulticast(SEARCH_DEVICES, &search);
        }

        void sendCancelSearch(const std::string& filter) {
          auto cancel = CancelSearchMsg();
          cancel.set_md_code(filter);
        
          sendMulticast(CANCEL_SEARCH, &cancel);
        }

      public:
        FakeApp(Context* context, peer_t id) : ModuleBase(context, id) {

          addFunction<AvailableDevicesMsg>(AVAILABLE_DEVICES, [this](MetaData metadata, AvailableDevicesMsg* m) -> void {
            dbg("Received available devices: ");
            auto size = m->devices_size();
            auto idx = 0;
            while (idx < size) {
              std::stringstream ss;
              ss << "  Device: {" 
                 << m->devices(idx).device_id() << ","
                 << m->devices(idx).hw_type() << ","
                 << m->devices(idx).hw_info() 
                 << "}";
              dbg("  " + ss.str());
              idx++;
            }
          });

          start();
        }

        void simulateSearchDevices(const std::string& filter) {
          dbg("Faking search devices: " + filter);
          sendSearchDevices(filter);
        }

        void simulateCancelSearch(const std::string& filter) {
          dbg("Faking cancel search: " + filter);
          sendCancelSearch(filter);
        }
      };

      class FakeMonicaModule : public ModuleBase {
        std::map<std::string, Device> connectedDevices;
        std::string filter = "";

        void sendFilteredDevices() {
          /*if (filter == "" || connectedDevices.size() == 0)
            return;*/
        
          auto filterResult = FilteredDeviceMsg();
          filterResult.set_md_code(filter);

          for (auto it = connectedDevices.begin(); it != connectedDevices.end(); it++) {
            filterResult.add_device_id(it->second.deviceId);
          }

          connectedDevices.clear();

          sendMulticast(FILTERED_DEVICE, &filterResult);
        }
      public:
        FakeMonicaModule(Context* context, peer_t id) : ModuleBase(context, id) {
        
          addFunction<DeviceConnectedMsg>(DEVICE_CONNECTED, [this](MetaData metadata, DeviceConnectedMsg* m) -> void {
            dbg("Monica connect: " + m->device_id());
            Device device("", m->device_id(), m->hw_type(), m->hw_info());
            connectedDevices.insert(std::pair<std::string, Device>(m->device_id(), device));

            sendFilteredDevices();
          });

          addFunction<DeviceDisconnectedMsg>(DEVICE_DISCONNECTED, [this](MetaData metadata, DeviceDisconnectedMsg* m) -> void {
            dbg("Monica disconnect: " + m->device_id());
            connectedDevices.erase(m->device_id());
          });

          addFunction<SetFilterMsg>(SET_FILTER, [this](MetaData metadata, SetFilterMsg* m) -> void {
            dbg("Set Filter: md_code: " + m->md_code());

            filter = m->md_code();
            sendFilteredDevices();
          });

          addFunction<ClearFilterMsg>(CLEAR_FILTER, [this](MetaData metadata, ClearFilterMsg* m) -> void {
            dbg("Clear Filter: md_code: " + m->md_code());
            filter = "";
          });

          start();
        }
      };
      
      class FlowDirector : public ModuleBase {
      public:
        FlowDirector(Context* context, peer_t id, FakePalModule* fakePal, FakeApp* fakeApp) : ModuleBase(context, id) {
          addStateCallback(RUNNING, [this, fakePal, fakeApp]() -> void {

            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            fakeApp->simulateSearchDevices("md_code_123");

            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            fakePal->simulateDeviceConnect("2222-2222", "type2", "info2");

            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            fakePal->simulateDeviceDisconnect("2222-2222");

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            fakePal->simulateDeviceConnect("1111-1111", "type1", "info1");

            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            fakeApp->simulateCancelSearch("md_code_123");

            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            requestShutdown(false);
          });

          start();
        }
      };
      /**********************************************************************/
      /*                                                                    */
      /*                                Main                                */
      /*                                                                    */
      /**********************************************************************/
      
      
      /**
       * @brief The proof-of-concept application.
       *
       * The application spins up the device manager and a fake PAL
       * module. A number of messages connecting and disconnecting devices
       * are simulated from the fake PAL.
       */
      int demo() {
        
        // Define zmq sockets and name of the Master module
        s4::BasePlate::BasePlate baseplate("inproc://module2reflector", "inproc://reflector2module", "Master");
        
        FakePalModule* fakePal = new FakePalModule(&baseplate.context, "PAL");
        FakeApp* fakeApp = new FakeApp(&baseplate.context, "App");

        baseplate.run({
          new DeviceManagerModule(baseplate.context, "DeviceManager"),
          fakePal,
          fakeApp,
          new FakeMonicaModule(&baseplate.context, "Monica"),
          new FlowDirector(&baseplate.context, "Flow", fakePal, fakeApp)}, 
          {},
          LogMessage_Level_DBG);
        
        return 0;
      }
    }
  }
}

// This will be hidden from the docs to avoid clutter
#ifndef __DOXYGEN__
// Redirect a global main function to the one found in the Demo
// namespace.
int main(void) {
  return s4::DeviceManager::Demo::demo();
}
#endif
