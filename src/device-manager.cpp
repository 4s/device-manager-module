/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief  Device Manager.
 *
 * @todo Write more.
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "device-manager.hpp"
#include "internal/internal-device-manager.h"

#include "Common.hpp"
#include "BasePlate.hpp"
#include "ModuleBase.hpp"

#include "message-topics.h"

#include <iostream>
#include <vector>
#include <sstream>


using namespace s4::BasePlate;
using namespace s4::messages;

#define LOGGER_LAMBDA [this](LogMessage_Level lvl, const std::string& msg, const std::string& file, int line) -> void { log(lvl, msg, file, line); }


namespace s4 {
  namespace device_manager {

    // ---------------------------------
    // -          Device impl          -
    // ---------------------------------
    class DeviceManager::impl {
     
    public:
      impl() { };
      
      InternalDeviceManager internalDeviceManager;
    };

    // ----------------------------------------------
    // -          DeviceManager impl          -
    // ----------------------------------------------
    DeviceManager::DeviceManager(Context& context) : ModuleBase(context, "DeviceManager"), pImpl(new impl()) {
      auto implCapture = pImpl; // share ownership between this and the lambdas
                                // capturing with '=' is deliberate to ensure pImpl.use_count is increased

      addFunction<device_control::DeviceConnected>(DEVICE_CONNECTED,    
        [=](MetaData meta, device_control::DeviceConnected* m) -> void { this->handleDeviceConnected(implCapture, m); });
      
      addFunction<device_control::DeviceDisconnected>(DEVICE_DISCONNECTED, 
        [=](MetaData meta, device_control::DeviceDisconnected* m) -> void { this->handleDeviceDisconnected(implCapture, m); });
     
      addFunction<device_management::SearchDevices>(SEARCH_DEVICES,      
        [=](MetaData meta, device_management::SearchDevices* m) -> void { this->handleSearchDevices(implCapture, m); });
      
      addFunction<device_management::CancelSearch>(CANCEL_SEARCH,         
        [=](MetaData meta, device_management::CancelSearch* m) -> void { this->handleCancelSearch(implCapture, m); });
      
      addFunction<dim::FilteredDevice>(FILTERED_DEVICE,       
        [=](MetaData meta, dim::FilteredDevice* m) -> void { this->handleFilteredDevice(implCapture, m); });
      
      start();
    }

    void DeviceManager::handleDeviceConnected(std::shared_ptr<impl> _impl, device_control::DeviceConnected* m) {
      _impl->internalDeviceManager.handleDeviceConnected(*m, LOGGER_LAMBDA);
    }

    void DeviceManager::handleDeviceDisconnected(std::shared_ptr<impl> _impl, device_control::DeviceDisconnected* m) {
      _impl->internalDeviceManager.handleDeviceDisconnected(*m, LOGGER_LAMBDA);
    }

    void DeviceManager::handleSearchDevices(std::shared_ptr<impl> _impl, device_management::SearchDevices* m) {
      auto msgToDIMs = _impl->internalDeviceManager.handleSearchDevices(*m, LOGGER_LAMBDA);
      if (msgToDIMs) 
        sendMulticast(SET_FILTER, msgToDIMs.get());
    }

    void DeviceManager::handleCancelSearch(std::shared_ptr<impl> _impl, device_management::CancelSearch* m) {
      auto msgToDIMs = _impl->internalDeviceManager.handleCancelSearch(*m, LOGGER_LAMBDA);
      if (msgToDIMs)
        sendMulticast(CLEAR_FILTER, msgToDIMs.get());
    }

    void DeviceManager::handleFilteredDevice(std::shared_ptr<impl> _impl, dim::FilteredDevice* m) {
      auto msgToApp = _impl->internalDeviceManager.handleFilteredDevice(*m, LOGGER_LAMBDA);
      if (msgToApp)
         sendMulticast(AVAILABLE_DEVICES, msgToApp.get());
    }
  }
}
