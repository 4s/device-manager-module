/*
 *   Copyright 2019 The 4S Foundation (www.4s-online.dk)
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


/**
 * @file
 * @brief  Internal Device Manager. A thin wrapper of std::vectors holding lists 
 *         of currently connected devices and ongoing queries for devices.
 *         Used as a pImpl-idiom in the 'real' device-manager partly to separate
 *         messaging-logic from device-logic and partly to make the device-logic
 *         easier to unit-test.
 *
 * @author <a href="mailto:claus.christiansen@alexandra.dk">Claus
 *         Christiansen</a>, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
#include "internal/internal-device-manager.h"

#include <iostream>
#include <vector>
#include <sstream>
#include <memory>

using namespace s4::BasePlate;
using namespace s4::messages;

using logCallback = std::function<void(LogMessage_Level, const std::string&, const std::string&, int)>;


#define dbg(message) logger(LogMessage_Level_DBG, message, __FILE__, __LINE__);
#define info(message) logger(LogMessage_Level_INFO, message, __FILE__, __LINE__);
#define warn(message) logger(LogMessage_Level_WARNING, message, __FILE__, __LINE__);
#define err(message) logger(LogMessage_Level_ERROR, message, __FILE__, __LINE__);
#define ftl(message) logger(LogMessage_Level_WARNING, message, __FILE__, __LINE__);



namespace s4 {
  namespace  device_manager {

    // ----------------------------------------------
    // -        InternalDeviceManager impl          -
    // ----------------------------------------------

    void InternalDeviceManager::handleDeviceConnected(const device_control::DeviceConnected& m, logCallback logger) {

      auto deviceId = m.device_id();
      if (connectedDevices.find(deviceId) != connectedDevices.end()) {
        warn("Connect of already connected device with deviceId: " + deviceId); 
        connectedDevices.erase(deviceId);
      }

      Device device("", m.device_id(), m.hw_type(), m.hw_info());
      std::string managedId =  getManagedId(device);
      if (managedId != "")
        device.managedId = managedId;

      connectedDevices.insert(std::pair<std::string, Device>(deviceId, device));
      dbg("Connected: " + device.toString()); 
    }

    void InternalDeviceManager::handleDeviceDisconnected(const device_control::DeviceDisconnected& m, logCallback logger) {
      auto deviceId = m.device_id();
      if (connectedDevices.find(deviceId) == connectedDevices.end()) {
        warn("Disconnect of unknown device with deviceId: " + deviceId);
      }     
      connectedDevices.erase(deviceId);
    }

    std::unique_ptr<dim::SetFilter> InternalDeviceManager::handleSearchDevices(const device_management::SearchDevices& m, logCallback logger) {
      auto filter = m.md_code();
      if (queries.find(filter) != queries.end()) {
        warn("Already handling query for: " + filter);
        return std::unique_ptr<dim::SetFilter>(nullptr); 
      }

      std::set<std::string> queryResults;
      queries.insert(std::pair<std::string, std::set<std::string>>(filter, queryResults));

      return makeSetFilter(filter, logger);
    }

    std::unique_ptr<dim::ClearFilter> InternalDeviceManager::handleCancelSearch(const device_management::CancelSearch& m, logCallback logger) {
      auto filter = m.md_code();
      if (queries.find(filter) == queries.end()) {
        warn("No query with filter: " + filter);
        return std::unique_ptr<dim::ClearFilter>(nullptr); 
      }

      queries.erase(filter);

      return makeClearFilter(filter, logger);
    }

    std::unique_ptr<device_management::AvailableDevices> InternalDeviceManager::handleFilteredDevice(const dim::FilteredDevice& m, logCallback logger) {
      auto filter = m.md_code();
      if (queries.find(filter) == queries.end()) {
        warn("Received results for non-existing/finished query");
        return std::unique_ptr<device_management::AvailableDevices>(nullptr); 
      }

      auto size = m.device_id_size();
      auto idx = 0;
      while (idx < size) {
        auto deviceId = m.device_id(idx);
        if (queries[filter].find(deviceId) == queries[filter].end()) {
          queries[filter].insert(deviceId);
          dbg("Query result added: {filter: " + filter + ", deviceId: " + deviceId + "}");
        }
        idx++;
      } 

      return makeAvailableDevices(filter, logger);
    }

    std::unique_ptr<dim::SetFilter> InternalDeviceManager::makeSetFilter(const std::string& filter, logCallback logger) {
      auto filterDevices = std::unique_ptr<dim::SetFilter>(new dim::SetFilter());
      filterDevices->set_md_code(filter);
      
      return filterDevices;
    }

    std::unique_ptr<dim::ClearFilter> InternalDeviceManager::makeClearFilter(const std::string& filter, logCallback logger) {
      auto filterClear = std::unique_ptr<dim::ClearFilter>(new dim::ClearFilter());
      filterClear->set_md_code(filter);

      return filterClear;
    }

    std::unique_ptr<device_management::AvailableDevices> InternalDeviceManager::makeAvailableDevices(const std::string& filter, logCallback logger) {
      if (queries.find(filter) == queries.end()) {
        warn("Received results for non-existing/finished query");
        return std::unique_ptr<device_management::AvailableDevices>(nullptr); 
      }

      auto available = std::unique_ptr<device_management::AvailableDevices>(new device_management::AvailableDevices());
      //FIXME: 
      //available->set_md_code(filter);
      
      for (auto it = queries[filter].begin(); it != queries[filter].end(); it++) {
        auto device = connectedDevices.find(*it);
        if (device == connectedDevices.end()) {
          warn("Device in query is not connected: " + *it);
        }
        else {
          auto added_device = available->add_devices();
          added_device->set_device_id(device->second.deviceId);
          added_device->set_hw_type(device->second.hardwareType);
          added_device->set_hw_info(device->second.hardwareInfo);
        }
      }

      queries[filter].clear();

      return available;
    }

    std::string InternalDeviceManager::getManagedId(const Device& device) {
      for (auto it = registeredDevices.begin(); it != registeredDevices.end(); it++) {
        // Comparing devices may be significantly more difficult; consider refactoring to
        // encapsulate comparison logic to new class
        if (device.hardwareType == it->second.hardwareType && device.hardwareInfo == it->second.hardwareInfo)
          return it->first;
      }

      return "";
    }

    std::map<std::string, Device> InternalDeviceManager::listRegisteredDevices() {
      std::map<std::string, Device> clone;
      clone.insert(registeredDevices.begin(), registeredDevices.end());
      return clone;
    }

    std::map<std::string, Device> InternalDeviceManager::listConnectedDevices() {
      std::map<std::string, Device> clone;
      clone.insert(connectedDevices.begin(), connectedDevices.end());
      return clone;
    }
      
    std::map<std::string, std::set<std::string>> InternalDeviceManager::listQueries() {
      std::map<std::string, std::set<std::string>> clone;
      clone.insert(queries.begin(), queries.end());
      return clone;
    } 
  }
}
