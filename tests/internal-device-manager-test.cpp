#include "gtest/gtest.h"

#include "internal/internal-device-manager.h"

#include "Common.hpp"
#include "BasePlate.hpp"


using namespace s4::BasePlate;
using namespace s4::DeviceManager;

#define DEVICE_MANAGER_TEST_SILENT_LOGGING

#define LOGGER_LAMBDA [this](LogMessage_Level lvl, const std::string& msg, const std::string& file, int line) -> void { testLogger(lvl, msg, file, line); }


namespace {
  void testLogger (LogMessage_Level lvl, const std::string& msg, const std::string& file, int line) {
    std::string lvls[] = { "FATAL ", "ERROR ", "WARN  ", "INFO  ", "DEBUG " };
#ifndef DEVICE_MANAGER_TEST_SILENT_LOGGING
    std::cout << lvls[lvl] << msg << "; " << file << " at line " << line << std::endl; 
#endif
  }
}

// Basic test-fixture with helper functions to generate messages
class InternalDeviceManagerTest : public ::testing::Test {
public:

  // Message from PAL -> DeviceManager: 
  std::unique_ptr<DeviceConnectedMsg> makeDeviceConnectedMsg(const std::string& id, const std::string& type, const std::string& info) {
    std::unique_ptr<DeviceConnectedMsg> m(new DeviceConnectedMsg());

    m->set_device_id(id);
    m->set_hw_type(type);
    m->set_hw_info(info);
    return m;
  }

  // Message from PAL -> DeviceManager: 
  std::unique_ptr<DeviceDisconnectedMsg> makeDeviceDisconnectedMsg(const std::string& id) {
    std::unique_ptr<DeviceDisconnectedMsg> m(new DeviceDisconnectedMsg());

    m->set_device_id(id);
    return m;
  }

  // Message from App -> DeviceManager: 
  std::unique_ptr<SearchDevicesMsg> makeSearchDevicesMsg(const std::string& filter) {
    std::unique_ptr<SearchDevicesMsg> m(new SearchDevicesMsg());

    m->set_md_code(filter);
    return m;
  }

  // Message from App -> DeviceManager: 
  std::unique_ptr<CancelSearchMsg> makeCancelSearchMsg(const std::string& filter) {
    std::unique_ptr<CancelSearchMsg> m(new CancelSearchMsg());

    m->set_md_code(filter);
    return m;
  }

  // Message from DIMs -> DeviceManager: 
  std::unique_ptr<FilteredDeviceMsg> makeFilteredDeviceMsg(const std::string& filter, const std::vector<std::string>& results) {
    std::unique_ptr<FilteredDeviceMsg> m(new FilteredDeviceMsg());
              
    m->set_md_code(filter);
    for (auto it = results.begin(); it != results.end(); it++) {
      m->add_device_id(*it);  
    }
    return m;
  }
};

// Test:
// * Connect device
// * Verify list of devices is updated
// * Verify values
TEST_F(InternalDeviceManagerTest, Can_Connect) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());

  auto connect = makeDeviceConnectedMsg("id", "type", "info");
  subject.handleDeviceConnected(*connect, LOGGER_LAMBDA);

  auto devices = subject.listConnectedDevices();
  EXPECT_EQ(1, devices.size());
  
  EXPECT_NE(devices.end(), devices.find("id"));

  auto device = devices.find("id")->second;
  EXPECT_EQ("id", device.deviceId);
  EXPECT_EQ("type", device.hardwareType);
  EXPECT_EQ("info", device.hardwareInfo);
}

// Test:
// * Connect device
// * Connect same device
// * Verify list of devices only contains 1
TEST_F(InternalDeviceManagerTest, Can_Connect_Same_Twice) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());

  auto connect = makeDeviceConnectedMsg("id", "type", "info");
  subject.handleDeviceConnected(*connect, LOGGER_LAMBDA);
  subject.handleDeviceConnected(*connect, LOGGER_LAMBDA);

  EXPECT_EQ(1, subject.listConnectedDevices().size());
}

// Test:
// * Disconnect device
// * Verify manager doesn't fail
TEST_F(InternalDeviceManagerTest, Can_Disconnect_NonConnected) {
  InternalDeviceManager subject;

  ASSERT_EQ(0, subject.listConnectedDevices().size());
  
  auto disconnect = makeDeviceDisconnectedMsg("id");
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);

  EXPECT_EQ(0, subject.listConnectedDevices().size());
}

// Test:
// * Connect device
// * Disconnect device
// * Verify list of devices is now empty
TEST_F(InternalDeviceManagerTest, Can_Connect_Then_Disconnected) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());

  auto connect = makeDeviceConnectedMsg("id", "type", "info");
  subject.handleDeviceConnected(*connect, LOGGER_LAMBDA);
  
  auto disconnect = makeDeviceDisconnectedMsg("id");
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);

  EXPECT_EQ(0, subject.listConnectedDevices().size());
}

// Test:
// * Connect device
// * Disconnect device
// * Disconnect device again
// * Verify list of devices is now empty
TEST_F(InternalDeviceManagerTest, Can_Disconnect_Twice) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());

  auto connect = makeDeviceConnectedMsg("id", "type", "info");
  subject.handleDeviceConnected(*connect, LOGGER_LAMBDA);
  
  auto disconnect = makeDeviceDisconnectedMsg("id");
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);

  EXPECT_EQ(0, subject.listConnectedDevices().size());
}

// Test:
// * Connect device1, device 2, device 2
// * Disconnect device2
// * Verify list of devices is correct
TEST_F(InternalDeviceManagerTest, Correct_Device_Is_Disconnected) {
  InternalDeviceManager subject; 
  
  ASSERT_EQ(0, subject.listConnectedDevices().size());

  auto connect1 = makeDeviceConnectedMsg("id1", "type1", "info1");
  subject.handleDeviceConnected(*connect1, LOGGER_LAMBDA);
  auto connect2 = makeDeviceConnectedMsg("id2", "type2", "info2");
  subject.handleDeviceConnected(*connect2, LOGGER_LAMBDA);
  auto connect3 = makeDeviceConnectedMsg("id3", "type3", "info3");
  subject.handleDeviceConnected(*connect3, LOGGER_LAMBDA);
  
  auto disconnect = makeDeviceDisconnectedMsg("id2");
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);

  auto devices = subject.listConnectedDevices();
  EXPECT_EQ(2, devices.size());
  EXPECT_NE(devices.end(), devices.find("id1"));
  EXPECT_EQ(devices.end(), devices.find("id2"));
  EXPECT_NE(devices.end(), devices.find("id3"));
}

// Test:
// * Query available matching filter
// * Verify list of queries is updated
TEST_F(InternalDeviceManagerTest, Can_Query) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(1, queries.size());
  
  EXPECT_NE(queries.end(), queries.find("md_code_123"));

  auto query = queries.find("md_code_123")->second;
  EXPECT_EQ(0, query.size());

  EXPECT_NE(nullptr, msgToDIMs.get());
  EXPECT_EQ("md_code_123", msgToDIMs->md_code());
}

// Test:
// * Query available matching filter
// * Query available matching another filter
// * Verify list of queries is updated
TEST_F(InternalDeviceManagerTest, Can_Query_Twice) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto search1 = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs1 = subject.handleSearchDevices(*search1, LOGGER_LAMBDA);

  auto search2 = makeSearchDevicesMsg("md_code_321");
  auto msgToDIMs2 = subject.handleSearchDevices(*search2, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(2, queries.size());
  
  EXPECT_NE(queries.end(), queries.find("md_code_123"));
  EXPECT_NE(queries.end(), queries.find("md_code_321"));

  EXPECT_NE(nullptr, msgToDIMs1.get());
  EXPECT_EQ("md_code_123", msgToDIMs1->md_code());

  EXPECT_NE(nullptr, msgToDIMs2.get());
  EXPECT_EQ("md_code_321", msgToDIMs2->md_code());
}

// Test:
// * Connect device1, device2
// * Query available matching filter
// * Add matching devices
// * Verify message to App
TEST_F(InternalDeviceManagerTest, Query_Results_Are_Sent_To_App) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());
  ASSERT_EQ(0, subject.listQueries().size());

  auto dev1 = makeDeviceConnectedMsg("dev1", "type", "info");
  subject.handleDeviceConnected(*dev1, LOGGER_LAMBDA);

  auto dev2 = makeDeviceConnectedMsg("dev2", "type", "info");
  subject.handleDeviceConnected(*dev2, LOGGER_LAMBDA);

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  std::vector<std::string> matches { "dev1", "dev2" };
  auto result = makeFilteredDeviceMsg("md_code_123", matches);
  auto msgToApp = subject.handleFilteredDevice(*result, LOGGER_LAMBDA);

  EXPECT_NE(nullptr, msgToApp.get());
  EXPECT_EQ("md_code_123", msgToApp->md_code());
  EXPECT_EQ(2, msgToApp->devices_size());
}

// Test:
// * Connect device1, device2
// * Query available matching filter
// * Disconnect device1
// * Add matching devices (both)
// * Verify message to App
TEST_F(InternalDeviceManagerTest, Query_Results_Are_Connected_Devices) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());
  ASSERT_EQ(0, subject.listQueries().size());

  auto dev1 = makeDeviceConnectedMsg("dev1", "type", "info");
  subject.handleDeviceConnected(*dev1, LOGGER_LAMBDA);

  auto dev2 = makeDeviceConnectedMsg("dev2", "type", "info");
  subject.handleDeviceConnected(*dev2, LOGGER_LAMBDA);

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  auto disconnect = makeDeviceDisconnectedMsg("dev1");
  subject.handleDeviceDisconnected(*disconnect, LOGGER_LAMBDA);

  std::vector<std::string> matches { "dev1", "dev2" };
  auto result = makeFilteredDeviceMsg("md_code_123", matches);
  auto msgToApp = subject.handleFilteredDevice(*result, LOGGER_LAMBDA);

  EXPECT_NE(nullptr, msgToApp.get());
  EXPECT_EQ("md_code_123", msgToApp->md_code());
  EXPECT_EQ(1, msgToApp->devices_size());
}

// Test:
// * Connect device1
// * Query available matching filter
// * Add matching devices (dev1)
// * Connnect device2
// * Add matching devices (dev2)
// * Verify message to App
TEST_F(InternalDeviceManagerTest, Query_Results_Sent_One_By_One) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listConnectedDevices().size());
  ASSERT_EQ(0, subject.listQueries().size());

  auto dev1 = makeDeviceConnectedMsg("dev1", "type", "info");
  subject.handleDeviceConnected(*dev1, LOGGER_LAMBDA);

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  std::vector<std::string> matches1 { "dev1" };
  auto result1 = makeFilteredDeviceMsg("md_code_123", matches1);
  auto msgToApp1 = subject.handleFilteredDevice(*result1, LOGGER_LAMBDA);

  auto dev2 = makeDeviceConnectedMsg("dev2", "type", "info");
  subject.handleDeviceConnected(*dev2, LOGGER_LAMBDA);

  std::vector<std::string> matches2 { "dev2" };
  auto result2 = makeFilteredDeviceMsg("md_code_123", matches2);
  auto msgToApp2 = subject.handleFilteredDevice(*result2, LOGGER_LAMBDA);

  EXPECT_NE(nullptr, msgToApp1.get());
  EXPECT_EQ("md_code_123", msgToApp1->md_code());
  EXPECT_EQ(1, msgToApp1->devices_size());
  EXPECT_EQ("dev1", msgToApp1->devices(0).device_id());

  EXPECT_NE(nullptr, msgToApp2.get());
  EXPECT_EQ("md_code_123", msgToApp2->md_code());
  EXPECT_EQ(1, msgToApp2->devices_size());
  EXPECT_EQ("dev2", msgToApp2->devices(0).device_id());
}

// Test:
// * Query available matching filter
// * Cancel query
// * Verify list of queries is updated
TEST_F(InternalDeviceManagerTest, Can_Cancel_Query) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs1 = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  auto cancel = makeCancelSearchMsg("md_code_123");
  auto msgToDIMs2 = subject.handleCancelSearch(*cancel, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(0, queries.size());

  EXPECT_NE(nullptr, msgToDIMs1.get());
  EXPECT_NE(nullptr, msgToDIMs2.get());
  // todo
}

// Test:
// * Query available matching filter
// * Cancel query with other filter
// * Verify list of queries is ok
TEST_F(InternalDeviceManagerTest, Can_Cancel_NonExisting_Query) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto search = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs1 = subject.handleSearchDevices(*search, LOGGER_LAMBDA);

  auto cancel = makeCancelSearchMsg("md_code_321");
  auto msgToDIMs2 = subject.handleCancelSearch(*cancel, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(1, queries.size());

  EXPECT_NE(nullptr, msgToDIMs1.get());
  EXPECT_EQ(nullptr, msgToDIMs2.get());
}

// Test:
// * Query available matching filter
// * New query with same filter
// * Verify this is not allowed
TEST_F(InternalDeviceManagerTest, Cannot_Reuse_Filter_In_Use) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto list = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs1 = subject.handleSearchDevices(*list, LOGGER_LAMBDA);
  auto msgToDIMs2 = subject.handleSearchDevices(*list, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(1, queries.size());

  EXPECT_NE(nullptr, msgToDIMs1.get());
  EXPECT_EQ(nullptr, msgToDIMs2.get());
}

// Test:
// * Query available matching filter
// * Cancel query
// * New query with same filter
// * Verify this is allowed
TEST_F(InternalDeviceManagerTest, Can_Reuse_Filter_After_Cancel) {
  InternalDeviceManager subject; 

  ASSERT_EQ(0, subject.listQueries().size());

  auto list = makeSearchDevicesMsg("md_code_123");
  auto msgToDIMs1 = subject.handleSearchDevices(*list, LOGGER_LAMBDA);

  auto cancel = makeCancelSearchMsg("md_code_123");
  subject.handleCancelSearch(*cancel, LOGGER_LAMBDA);

  auto msgToDIMs2 = subject.handleSearchDevices(*list, LOGGER_LAMBDA);

  auto queries = subject.listQueries();
  EXPECT_EQ(1, queries.size());

  EXPECT_NE(nullptr, msgToDIMs1.get());
  EXPECT_NE(nullptr, msgToDIMs2.get());
}
